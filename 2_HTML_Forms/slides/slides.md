# Frontend Web Development

Quentin Richaud

qrichaud.pro@gmail.com

---

# In order to see this presentation and code example

## Locally

You can checkout the repository with the presentation and code examples from Gitlab :

https://gitlab.com/emse1/cours-dev-web-2

You can read the instructions in the root folder `README.md`. You can 
run the `run.sh` script which will launch a simple python web server (python3 must be installed), serving
all the files present in this folder, and allowing you to display the webpage
examples, with all assets (CSS, images, fonts) being correctly loaded.

You can now access http://localhost:8000/ in your browser.

The slides are at http://localhost:8000/2_HTML_Forms/slides/presentation.html

---

# In order to see this presentation and code example

## Online

The slides are avaible here : https://emse1.gitlab.io/cours-dev-web-2/2_HTML_Forms/presentation.html

---

# HTML Forms

Forms in HTML are meant to provide more interaction with the user.

Without forms, the only thing the user can do is read the content displayed by the HTML page, and navigate from 
one page to another using the hyperlinks.

Forms provide tools for the user to input data, and send back this data to the web server.

---

# Client/Server interaction without forms

![](./imgs/client_server_interaction1.png)


---

# Client/Server interaction with forms

![](./imgs/client_server_interaction2.png)


---

# Exemple of an HTML document with a form

See code example 1, for a demo of the most common Form elements.

---

# Anatomy of a form element

![](./imgs/input_anatomy.png)

---

# How the data is sent to the server

![](./imgs/form_tag_anatomy.png)

---

# How the data is sent to the server

When you submit the form (can be either by clicking on the "submit" button of the form, or by pressing ENTER in a field of the form),
a request is sent to the server using  :

- The URL specified in the `action` attribute (default : same URL than the current page)
- The HTTP method specified in the `method` attribute

Concerning HTTP methods, the good practice is to use `GET` when you request data from the server (asking for a webpage for example), 
and `POST` when you submit data to the server (submitting a form).

Anymay, this will be determined by the implementation of your backend (the server software),
not your frontend. The frontend just needs to follow the backend's specification.

---

# Data transmitted to the server on a form submission

The native HTML way of transmitting form data is to use the HTTP specific data payload 
format corresponding to the HTTP method used for the request.

For a GET request : the data is transmitted in the URL as URL paramters. Example :

![](./imgs/get_request_with_data.png)

For a POST request : the data is transmitted in the body of the request, in an HTTP specific format,
which can then be retrieved by the server. Example :

![](./imgs/post_request_data_example.png)

You can inspect the request body in the web browser developper tools.

---

# Native data transmittion vs. javascript

The native way of transmitting data using form is historically the main method to transmit data.
However it as some limitations :

- A page reload is mandatory. Each data submission means page reload.
- You use another user interaction than form submission to transmit data
- You can choose the way the data is formatted (for example, JSON or XML data).

That's why nowaday is is common to use javascript to handle data transmission to the server
(in addition to native forms, or using exclusively javascript).

---

# Form validation

HTML provides tools to validate the data before sending the form. Here is one example with
the text input :

![](./imgs/example_text_input_with_pattern.png)

Here we used the `pattern` attribute, which allows us to specify a Regular Expression (regex) 
that the input value must follow in order to be valid.

See example2

---

# Form validation

An invalid form has 2 effects :

- It forbids the submission of the form (the browsers natively displays an error message)
- It gives you access to the `:invalid` CSS pseudo class, on the form and on the input that is not valid,
  in order to display custom styling.

By default, you also have the `:valid` CSS pseudo class on valid form and inputs. 

See example2.

---

# Form validation

Native form validation has limitations. (For example : you can't apply CSS styling for invalid 
fields only after a submission attempt).

It is common to use Javascript to do more advanced validation. 

---

# Form validation and security

Form validation on the frontend is not a security measure. There are other tools that allow you
to send whatever data you want to the server,
(one could edit the HTML in the developper tools for example, or send hand crafted data with 
the `curl` command, …)

Form validation is meant as a User Experience (UX) improvment.

For security, you must implement data validation on the backend.

